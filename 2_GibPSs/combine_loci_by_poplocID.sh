#!/bin/bash
# Annelies Haegeman 2021
# annelies.haegeman@ilvo.vlaanderen.be

#Specify here the folder where your loci files are:
DIR=/home/genomics/ahaegeman/GBS_Ilex/Gibpss

date
echo

# Change directory to specified folder
cd $DIR

# Define the variable FILES that contains all the files of interest
# When you make a variable, you can not use spaces! Otherwise you get an error.
FILES=( *_loci.txt )

#Loop over all files and select the loci of interest (final loci list)
for f in "${FILES[@]}" 
do 

	#Define the variable SAMPLE who contains the basename where the extension is removed (-1.fastq.gz)
	SAMPLE=`basename $f _loci.txt`
	echo "PROCESSING $SAMPLE...."
	
	#now select the loci of interest for that individual by selecting only the "valid" loci
	sed -n "1p;/"valid"/p" "$SAMPLE"_loci.txt > "$SAMPLE"_nolost_loci.txt
	
	#make a file with the poplocIDs linked to the individual loci. This is done by looking at the "indpoploc.txt" file and storing the second column (poplocID, $2) in a hash "h" with as key the first column (indlocID, $1).
	#Next the loci.txt file is checked and from this we print the filename, the poplocID as found in the hash and some of the columns of the loci file.
	awk 'NR==FNR {h[$1] = $2; next} {print FILENAME,h[$1],$1,$2,$3,$4,$5}' "$SAMPLE"_indpoploc.txt "$SAMPLE"_nolost_loci.txt| tail -n +2 | sort -n -k2 > "$SAMPLE"_allinfo.txt
	
	#remove "no_lost_loci.txt file"
	rm "$SAMPLE"_nolost_loci.txt
	
	#store all the filename of the resulting file in a variable (to cat everything later)
    ALLSAMPLES+=""$SAMPLE"_allinfo.txt "
done

#concatenate all resulting files
cat $ALLSAMPLES > poplocinfo_all_samples.txt