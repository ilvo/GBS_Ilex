#!/usr/bin/perl

# (c) Annelies Haegeman (P39)
# Version 1, 20/01/22
# Description: this Perl script reads a fastafile and outputs the same fasta file with new names.
# Input file: a fasta file and a file with old names and new names (tab separated)
# Output file: a fasta file with the new names as ID
# Usage: perl rename.pl inputfile.fa renamefile.txt outputfile.fa
# Remark: make sure you first do dos2unix inputfile.fa before you use this script in a Linux environment
# Installation requirements: Perl, Bioperl

#----------------------------------------------------------------------------------

$|=1;
use strict;
use Bio::SeqIO;
use Data::Dumper;
use warnings;

################
#set parameters#
################

my ($sequence_object, $id, $desc, $seq,$substring,$lijn,@line,%hash);
die  "usage rename_fasta.pl input.fa renamefile.txt output.fa" if (scalar @ARGV != 3);
my $inputfile= $ARGV[0];
my $renamefile= $ARGV[1];
my $output= $ARGV[2];
open (OP,">$output");

#################
#read rename file#
#################
open (IP,$renamefile) || die "cannot open \"$renamefile\":$!";
while ($lijn=<IP>)	
	{	chomp $lijn;
		@line = split (/\t/,$lijn);
		$hash{$line[0]}=$line[1];
	}

#################
#read fasta file#
#################
my $seqio = Bio::SeqIO -> new (
				'-format' => 'fasta',
				'-file' => $inputfile
				);
				
#####################################################
#go through all sequences of fasta file and print id#
#####################################################
while ($sequence_object	 = $seqio -> next_seq) {
	$id=$sequence_object->id();	#id
	#$desc=$sequence_object->desc();	#description
	$seq=$sequence_object->seq();	#sequence

	print OP ">$hash{$id}\n";
	print OP "$seq\n";
	}

close (OP);