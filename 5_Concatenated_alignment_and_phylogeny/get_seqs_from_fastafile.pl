#!/usr/bin/perl

# (c) Annelies Haegeman (P21)
# Version 1, 13/12/11
# Description: this Perl script reads a fastafile and a file containing IDs of the sequences you want to extract from the fasta file. It prints the selected sequences to a new fasta file.
# Input file 1: a text file containing sequence IDs you want to select from the fasta file, each ID on a new line (ids.txt)
# Input file 2: a fasta file (input.fa)
# Output file: a fasta file containing the selected sequences.
# Usage: perl get_seqs_from_fastafile.pl ids.txt input.fa output.fa
# Remark: make sure you first do dos2unix on the input files before you use this script in a Linux environment
# Installation requirements: Perl, Bioperl

#----------------------------------------------------------------------------------

$|=1;
use strict;
use Bio::SeqIO;
use Data::Dumper;
#use warnings;

die  "usage get_seqs_from_fastafile.pl ids.txt input.fa output.fa" if (scalar @ARGV != 3);

################
#set parameters#
################
my ($inputfile,$i,$k,$sequence_object,$lijn,@array,@line,%hash,$datafile,$id,$seq,$l,$descriptor);
$i=0;$k=0;

#############
#input files#
#############
my $inputfile= $ARGV[0];
my $datafile = $ARGV[1];
my $outputfile = $ARGV[2];

############################################
#lees ids van eerste file en steek in array#
############################################
open (IP,$inputfile) || die "cannot open \"$inputfile\":$!";
while ($lijn=<IP>)		
	{  	@line = split (/\s/,$lijn);
		push (@array,$line[0]);		#steek ids van file 1 in @array
		#$hash{$line[0]}=$line[1];
		$i++;
	}

#####################################################################
#overloop array met ids, indien gelijk aan id van tweede file, print#
#####################################################################
my $seqio = Bio::SeqIO -> new (
				'-format' => 'fasta',
				'-file' => $datafile
				);

open (IP,$datafile) || die "cannot open \"$datafile\":$!";
open (OP,">$outputfile");

while ($sequence_object	 = $seqio -> next_seq) {
		$id = $sequence_object->id;		#id
		$seq = $sequence_object->seq;		#sequence		
		#$descriptor = $sequence_object->desc;	#description
		#$l = length($seq);			#length
		
		while ($k<$i){				
			#print "$array[$k]\n";
			if ($array[$k] eq $id ){
				print OP ">$id\n";
				#print "$descriptor\t";
				#print "$l\t";
				print OP "$seq\n";			
				}
			$k++;
			}
		$k=0;	
		}