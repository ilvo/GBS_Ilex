#!/usr/bin/perl

$|=1;
use strict;
#use warnings;
use Bio::AlignIO;
use Bio::SeqIO;
use Getopt::Long;
use Pod::Usage;
use List::Util qw( min max );

########
#manual#
########
=head1 NAME

convert_genotypes_to_concatenated_alignment.pl - converts the Gibpss genotypes.txt file to a concatenated alignment (unfiltered)

=head1 SYNOPSIS

convert_genotypes_to_concatenated_alignment.pl  -input | i <filename> -loc | l <filename>

=head1 DESCRIPTION

convert_genotypes_to_concatenated_alignment.pl will convert the Gibpss genotypes.txt file to a concatenated alignment in fasta format.
The script also takes the locdata.txt file from Gibpss as input to keep track of which loci and bases are printed in the alignment.
No filtering steps are done, BUT only loci are kept which have 1 or more SNPs as mentioned in the locdata file.
An output file containing the positional information (called "info_positions_unfiltered_alignment.txt") and a fasta file of the concatenated alignment (called "concatenated_alignment_unfiltered.fasta") are automatically generated when running the script.

=head1 COMMAND LINE OPTIONS AND ARGUMENTS

Description of options

=head2 Mandatory arguments

=over

=item -input | i <filename>

The input file is the "genotypes.txt" file as it is produced by the software Gibpss.

=item -loc | l <filename>

The loc file is the "locdata.txt" file as it is produced by the software Gibpss.


=back

=head1 BUG REPORTS & COMMENTS

Please send an e-mail to annelies.haegeman@ilvo.vlaanderen.be

=cut

# print manual if no options given
if (!$ARGV[0]) {
	print STDERR `pod2text $0`;
	exit;
}

#############
#input files#
#############
# parse command line
my ($datafile, $outputfile,$locdatafile,$gap,$single,$a);
GetOptions	(
				'input|i=s' => \$datafile,			#s = string
				'loc|l=s' => \$locdatafile,
			 	'<>' => \&other_options
			);

# check for mandatory options
pod2usage( { -message => q{One or more mandatory argument(s) is missing!} ,
             -exitval => 1  ,
             -verbose => 1
#           #} ) unless ($datafile and $outputfile and $gap and $locdatafile and $a);
} ) unless ($datafile and $locdatafile);



#####################
#initiate variabeles#
#####################
#print "$single\n";
my ($lijn,@line,@names,$length,@elementlist,%seqs,$empty,$id,@locinfo,%sl,%cons,%nSNP,%varpos,%nall,%nInd,$poplocID,$position,@vars,$numbers);
my ($sequence_object1,$seqio1,$seq,@seqarray,$length,$element,%count,@freqs,$min,$max,@finalpositions,$keep,$id,@arrayseq,$pos,$k,$v,$origpos,$newpos,@keptloci,%counts,@unique,$nrloci,@nonACGTcharacters);
my $numseq=0;
my $numberofgaps=0;
my $numberofaberrants=0;
my $string="";
my $i=0;
my $j="-999";
my $k=0;
my $l=0;
my $totallength=0;
my $totalloci=0;
open(OP, ">concatenated_alignment_unfiltered.fasta");
open(OP2,">info_positions_unfiltered_alignment.txt");
print OP2 "position_unfiltered_alignment\tpoplocID\tvarpos\tlocuslength\tnSNP\tn_all\tnInd\tcons\n";


##############################################################################
#SCRIPT PART1: CONSTRUCT UNFILTERED CONCATENATED ALIGNMENT FROM GIBPSS OUTPUT#
##############################################################################

print "\nConverting genotypes file to concatenated alignment....\n\n";

#read locdatafile and store information in a hash with as key the poplocID and as value the information of interest
open (IP,$locdatafile) || die "cannot open \"$locdatafile\":$!";
while ($lijn=<IP>)
	{	chomp $lijn;
		next if $. < 2; # Skip first line
		#poplocID	sl	cons	nSNP	varpos	n_all	nInd	nIndloc	ties
		@locinfo = split (/\t/,$lijn); #split line into fields
		$sl{$locinfo[0]}=$locinfo[1];		#store relevant info in hashes with as key the poplocID
		$cons{$locinfo[0]}=$locinfo[2];
		$nSNP{$locinfo[0]}=$locinfo[3];
		$varpos{$locinfo[0]}=$locinfo[4];
		$nall{$locinfo[0]}=$locinfo[5];
		$nInd{$locinfo[0]}=$locinfo[6];
		#print "$locinfo[0]\t";
		#print "$nSNP{$locinfo[0]}\n";
	}
close(IP);

#read genotypes file and loop over all lines (=loci)
open (IP2,$datafile) || die "cannot open \"$datafile\":$!";
while ($lijn=<IP2>)
	{	chomp $lijn;
		if ($. == 1){		#store names of isolates in first line
			@names = split (/\t/,$lijn);
			shift(@names); #remove the first element (which is "poplocID")
		}
		else{	#from second line on, make a file per locus containing the sequences from all samples

			#prepare data
			@line = split (/\t/,$lijn);
			$poplocID=$line[0];  #store poplocID in $poplocID
			shift(@line); #remove the first element (which is the locus name, the locus name was stored in $poplocID)

			#check if the locus had SNPs, and only continue if SNPs were present (nSNP>0)
			if ($nSNP{$poplocID}>0){
				#keep track of the number of loci which contain SNPs
				$totalloci++;
				#determine the total length of the SNPs of the locus by looping over all elements of a line, and when the element is different than -999, save the length
				$length=$nSNP{$poplocID};
				#print "$poplocID\t";
				#print "$length\n";

				#loop over the elements (=sequences) in the line and add to hash with as ID the sample name and as value the previous value with the new value concatenated
				foreach my $element (@line){
					if ($element eq "consensus"){

					}
					elsif ($element ne "-999") {
						@elementlist = split (/\//,$element);
						if (scalar(@elementlist) == 1) {	#if only 1 allele
							$seqs{$names[$i]}=$seqs{$names[$i]}.$elementlist[0];	#hash with as key the name of the sample and as value the concatenated sequence
							}
						else {	#if more than 1 allele
							#open TEMP file to store sequence data of the alleles in fasta format
							open (TEMP,">temp.fasta");
							foreach my $allele (@elementlist){
								print TEMP ">$k\n";
								print TEMP "$allele\n";
								$k++;
							}
							close(TEMP);

							#now calculate consensus sequence from alignment
							my $consensus = lc(consensus("temp.fasta"));

							#write consensus sequence to hash
							$seqs{$names[$i]}=$seqs{$names[$i]}.$consensus;

							#initiate variables, make TEMP file empty
							$k=0;
							system("rm temp.fasta");
						}
					}
					else {	#if -999, add "-" for the length of the locus
						#print OP ">$names[$i]\n";
						$empty="-" x $length;
						#print "$empty\n";
						$seqs{$names[$i]}=$seqs{$names[$i]}.$empty;
					}
					$i++;	#counter variable
				}

				#keep track of the positions: which position is derived from which locus? Write this info to a new output file, including some extra info on the locus itself
				#first put the elements of varpos "p:30/31" of the specific locus in a separate list
				$numbers=substr($varpos{$poplocID},2);	#take the variable positions of the locus, and remove the first two characters ("p:")
				@vars = split (/\//,$numbers);
				#next loop over all positions in the locus and write relevant info to an output txt file
				while ($l<$length){
					$position=$totallength+$l+1; 	#position in alignment = total length of the alignment so far + current base in the loop (=position in current locus) + index 1
					print OP2 "$position\t";
					print OP2 "$poplocID\t";
					print OP2 "$vars[$l]\t";	#extract the correct position from the @vars list
					print OP2 "$sl{$poplocID}\t";
					print OP2 "$nSNP{$poplocID}\t";
					print OP2 "$nall{$poplocID}\t";
					print OP2 "$nInd{$poplocID}\t";
					print OP2 "$cons{$poplocID}\n";
					#go to next position in alignment
					$l++;
					}


				#calculate the total length of the alignment so far
				$totallength=$totallength + $length; #length of the alignment so far + length of the last added locus

				#after finishing processing the line, but the counters back at zero
				$i=0;
				$l=0;
			}
		}
	}

close(IP2);

#when finishing reading the data, print the resulting hash %seqs to a fasta formatted file
foreach $id (keys %seqs) {
    print OP ">$id\n";
    print OP "$seqs{$id}\n";
}

#Report that the script has finished, and some metrics
print "\nFinished making the unfiltered concatenated alignment.\n";
print "The unfiltered alignment is $totallength bases long and is derived from $totalloci different loci.\n\n";


########################################################################
#FUNCTIONS

#function "consensus" with as argument a fasta file with aligned sequences, the function returns the consensus sequence with iupac code
sub consensus {
	my $seqname = "consensus";
	my $in  = Bio::AlignIO->new(-file   => @_ ,
                          -format => 'fasta');
	my $ali = $in->next_aln();
	my $iupac_consensus = $ali->consensus_iupac();   # dna/rna alignments only
	my $seq = Bio::Seq->new(-seq => "$iupac_consensus",
                        -display_id => $seqname);
	my $seq2 = $seq->seq;
	return $seq2;
}
