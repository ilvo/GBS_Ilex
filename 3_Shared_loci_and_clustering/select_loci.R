#set working directory
#setwd("/home/genomics/ahaegeman/GBS_Vroedmeesterpad/PstI-MspI/export1")
#setwd("/home/genomics/ahaegeman/GBS_Vroedmeesterpad/PstI-MseI/export1")
#setwd("/home/genomics/ahaegeman/GBS_Vroedmeesterpad/EcoRI-MseI/export1")
#setwd("/home/genomics/ahaegeman/GBS_Vroedmeesterpad/EcoRI-MspI/export1")
setwd("/home/genomics/ahaegeman/GBS_Vroedmeesterpad/EcoRI-ApeKI/export1")

#load required packages
library(data.table)
library(ggplot2)

options(max.print=1000)#to prevent R from printing results

#################
#READ DATA
#read genotypes input as data.table (an enhanced data.frame)
genotypes<-fread(input="genotypes.txt",sep="\t",header=TRUE)

#FIX NAMES in genotypes file
#add "Sample" (from isolate) in front of all sample names because the data.table can only cope with names starting with characters
names<-colnames(genotypes)
newnames <- paste("Sample_", names, sep="")
#reset the headernames in the data.table
newnames[1]<-"poplocID"
#replace the hyphens by underscores. It cannot handle names with more than 1 hyphen, or a hyphen combined with an underscore
newnames2<-gsub("-", "_", newnames)
colnames(genotypes)<-newnames2

#################
#SELECT GENOTYPES OF INTEREST
#Here we want to find all loci which are in common for samples of interest

#make a subset of the genotypes file only including the samples of interest 
colnames(genotypes)
genotypes_subset<-genotypes[,c("poplocID","Sample_20_007218_PoelQ_staartpuntjes_EcoRI_ApeKI","Sample_20_007235_PoelU_staartpuntjes_EcoRI_ApeKI","Sample_E2020STF103_PoelQ_larvewater_EcoRI_ApeKI"),with=FALSE]
#genotypes_subset<-genotypes[,c("poplocID","Sample_20_007218_PoelQ_staartpuntjes_EcoRI_MspI","Sample_E2020STF103_PoelQ_larvewater_EcoRI_MspI"),with=FALSE]
#genotypes_subset<-genotypes[,c("poplocID","Sample_20_007218_PoelQ_staartpuntjes_EcoRI_MseI","Sample_E2020STF103_PoelQ_larvewater_EcoRI_MseI"),with=FALSE]
#genotypes_subset<-genotypes[,c("poplocID","Sample_20_007218_PoelQ_staartpuntjes_PstI_MseI","Sample_20_007235_PoelU_staartpuntjes_PstI_MseI","Sample_E2020STF103_PoelQ_larvewater_PstI_MseI","Sample_E2020STF101_PoelU_larvewater_PstI_MseI"),with=FALSE]
#genotypes_subset<-genotypes[,c("poplocID","Sample_20_007218_PoelQ_staartpuntjes_PstI_MspI","Sample_E2020STF103_PoelQ_larvewater_PstI_MspI"),with=FALSE]

colnames(genotypes_subset)
#write.table(genotypes_subset,file="genotypes_subsetinds.txt",quote=F,sep="\t",row.names=F,col.names=T)

#################
#SELECT LOCI THAT ARE SHARED BETWEEN INDIVIDUALS
#set poplocID as key
setkeyv(genotypes_subset,"poplocID")

#replace values of "genotypes.txt" to 0 and 1 (absent vs present), except in column 1 which has the poplocID (names(genotypes)[-1])
for(col in names(genotypes_subset)[-1]) set(genotypes_subset, i=which(genotypes_subset[[col]]!="-999"), j=col, value=1)
for(col in names(genotypes_subset)[-1]) set(genotypes_subset, i=which(genotypes_subset[[col]]=="-999"), j=col, value=0)
#make numeric
genotypes_subset[, names(genotypes_subset) := lapply(.SD, as.numeric)]
#check
sapply(genotypes_subset, class)  #all columns should be numeric
min(genotypes_subset)  #the minimum values should be 0 and not -999

#Add a column SUM which contains the sum of all columns (except poplocID)
genotypes_subset[, `:=`(SUM = rowSums(.SD, na.rm=T)), .SDcols=-c("poplocID")]
#now store all loci numbers where the SUM is equal to the number of isolates (i.e. locus is present in all isolates)
sharedloci<-genotypes_subset[ SUM == (length(colnames(genotypes_subset))-2), "poplocID"] # number of columns -2 (poploc ID and SUM) is number of isolates
#if you also want to include loci that have data for for example >70% of the isolates, use the following:
#sharedloci90<-genotypes_subset[ SUM >= (length(colnames(genotypes_subset))-2)*0.90 , "poplocID"]

#Write loci numbers to output
sharedloci.list<-as.vector(sharedloci$poplocID)
write(sharedloci.list,file="sharedloci.txt",sep="\n")


##################
#READ DATA FROM LOCDATA FILE, SUBSELECT LOCI OF INTEREST

#read locdata file as data.table (an enhanced data.frame)
locdata<-fread(input="locdata.txt",sep="\t",header=TRUE)
#set poplocID as key
setkeyv(genotypes_subset,"poplocID")

#make subset of locdata with loci of interest
locdata_subset<-locdata[poplocID %in% sharedloci$poplocID,]

#make histogram of lengths of selected loci
plot<-ggplot(data=locdata_subset, aes(locdata_subset$sl)) + 
  geom_histogram(bins=50) +
  xlab("locus length")
pdf(file="histogram_length_sharedloci_20-007218_PoelQ_staartpuntjes_PstI-MspI_E2020STF103_PoelQ_larvewater.pdf")
plot
dev.off()

#check how many loci have a length of >= 100 bp
locdata_subset_largerthan100bp<-locdata_subset[sl >= 100,]

#plot histogram of number of SNPs of loci >= 100 bp
plot<-ggplot(data=locdata_subset_largerthan100bp, aes(locdata_subset_largerthan100bp$nSNP)) + 
  geom_histogram(bins=50) +
  xlab("nSNP")
pdf(file="histogram_nSNPs_sharedlocilargerthan100_20-007218_PoelQ_staartpuntjes_PstI-MspI_PoelQ_larvewater_PstI-MspI_E2020STF101.pdf")
plot
dev.off()

#read loci file with loci you want to subselect
#loci<-readLines("/home/genomics/kvanpoucke/Gibpss_Phytophthora_5/phylogeny/all_clades/sharedloci_30.txt")
#make a subset of the data with only the rows for which the poplocID is in the variable "loci"
#genotypes_subset2<-genotypes_subset[list(as.integer(loci))]


#########################################

#FINAL SELECTION OF INTERESTING LOCI

#run lines 11-80 of the code above with the correct individuals and RE combination

#select loci > 100 bp with 1-7 SNPs
locdata_subset_largerthan100bp_1_7SNPs<-locdata_subset_largerthan100bp %>% filter(nSNP>=1) %>% filter(nSNP<=7)
dim(locdata_subset_largerthan100bp_1_7SNPs)
#rename poplocID to include enzyme combination
locdata_subset_largerthan100bp_1_7SNPs$poplocID<-paste0("EcoRI-ApeKI_",locdata_subset_largerthan100bp_1_7SNPs$poplocID)
head(locdata_subset_largerthan100bp_1_7SNPs)
#write to file
write.table(locdata_subset_largerthan100bp_1_7SNPs,file="EcoRI-ApeKI_sharedloci_largerthan100bp_1-7SNPs.txt",quote=F,sep="\t",row.names=F,col.names=T)
