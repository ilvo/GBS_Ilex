# GBS Ilex

In this project, we want to study the genetic variation within and between some accessions of the genus Ilex using Genotyping-by-sequencing (GBS).

### 1. Preprocessing of GBS data

Preprocessing of the data was done using the [GBprocesS](https://gbprocess.readthedocs.io) workflow.
Briefly, the following steps were done:
- demultiplexing
- trimming of adapters and remains of restriction site
- merging of forward and reverse reads
- quality filtering (including filtering reads that contain N's)
- removal of reads that contain internal restriction sites

The ini and barcode files for GBprocesS are in this repository in the folder "1_GBprocesS".

Run GBprocesS as follows:

`gbprocess --config Preprocess_2601_P01.ini`
`gbprocess --config Preprocess_2601_P02.ini`

### 2. Reference-free data-analysis using GibPSs

Since there is no reference genome available and since we are working with accessions from multiple species, we process the data using the reference-free GBS analysis software [GibPSs](https://github.com/ahapke/gibpss). All files needed for the GibPSs analysis are in the folder "2_GibPSs".

#### 2.1 Identification of loci using indloc, poploc and indpoploc

##### 2.1.1 indloc
The `indloc` program identifies loci within individuals.
First we create a file called `infiles.txt`, which contains a list of all input files to be processed. This is a tab-delimited file, with on each line the sample ID and the filename (separated by a tab). We also need a file called `ind_distance_settings.txt`. This is a text file with three integers per line, separated by tabs, corresponding to the sequence length, the "good_svar_d" (maximum distance allowed between neighboring good sequence variants) and the "rare_svar_d" (maximum distance allowed between a rare sequence variant and at least one good svar in a locus network). For more information, you can read the ["indloc" manual page](https://github.com/ahapke/gibpss/blob/master/docu/05_docu_indloc_23.0.pdf) of Gibpss.

Make sure that the input fastq files are unzipped. Here we unzip using 16 processors in parallel.
`pbzip -p16 *.bz2`

Next, run the `indloc` program.

`indloc_23.0.pl -i infiles.txt -d ind_distance_settings.txt -D 5 -mdl 10 -P 24 -r 0 -M f -c 0.2 -a 0.2`

The following options are used:
`-i` : file with list of inputfiles: tab-delimited file with sample ID and filename
`-d` : file with distance settings
`-D` : minimum depth of good sequence variants
`-mdl` : minimum depth of a locus
`-P` : maximum number of processes in parallel
`-r` : in case of 0 : do not build reverse complement duplicates
`-M` : method for SNP calling, in case of `f` : frequency threshold method
`-c` : charachter frequency threshold in the frequency threshold method, in case of 0.2, valid characters must have a minimum depth corresponding to 20% of the total number of reads in the locus.
`-a` : allele frequency threshold in the frequency threshold method: additional filtering step of alleles where, in case set to 0.2, alleles occurring at a depth less than 20% of the total depth are discarded.

##### 2.1.2 poploc
The `poploc` program identifies loci based on all individuals (the population) in the dataset. It clusters all alleles from all individuals into loci according to the distance criteria used. We also need a file called `pop_distance_settings.txt`. This is a text file with two integers per line, separated by tabs, corresponding to the sequence length, and the "interind_d", or the maimum distance two alleles can have to be clustered. For more information, you can read the ["poploc" manual page](https://github.com/ahapke/gibpss/blob/master/docu/06_docu_poploc_09.0.pdf) of Gibpss.

`poploc_09.0.pl -msl 0 -d pop_distance_settings.txt`

The following options are used:
`-msl` : in case of 0, inactivate merging of overlapping fragments
`-d` : file with distance settings


##### 2.1.3 indpoploc
The `indpoploc` program links the results from `indloc` and `poploc`. Make sure to run if from the folder with the `indloc` and `poploc` results.

`indpoploc_06.0.pl`

#### 2.2 Remove loci with insertion/deletions (indels)

We first need to filter loci that have a read length shorter than 32. This is because the `usearch` program, which is used to find related loci with indels, can only handle sequences longer than 32 bp. Of course you can omit this step if during the preprocessing you already selected a final minimum lenght of (higher than) 32 after the merging step.

To select loci larger than 32 bp, we run the `data_selector` and select first option 2 "select loci", then 3 "Sequence length", set the length borders (min 32, max 300) and finally confirm with option 13 "Run".

`data_selector_16.0.pl`

Next we can run `indel_checker`. With the option `-UP` you have to give the path to the `usearch` program, and with the option `-DP` you need to specify the path to the working directory (for `usearch`).

`indel_checker_03.0.pl -UP /usr/local/bin/usearch -DP /home/genomics/ahaegeman/working_directory`

Using the `data_selector` we can now remove the indels by selection option 2 "select loci", then 2 "List of loci in a file", enter `./indelcheck/no_indel_loc.txt` as filename and confirm with 13 "Run".

`data_selector_16.0.pl`

#### 2.3 Depth analyzer

In this step, loci with an unproportional high amount of sequences are removed since these can be the result of for example paralogues. As input file, we give the `lengths_for_depth_analyzer.txt` file, which .

`depth_analyzer_09.0.pl lengths_for_depth_analyzer.txt`

Next, we plot the results in R and select appropriate cut-offs for "max med_scaldep" and "min med_dep_perc". This can be done by running an R script on the terminal, and giving the name of the output file of depth analyzer as argument (-c) and the name of the folder where that file is as argument (-d).

`Rscript depth_analyzer_graphs.R -c da_all_loc.txt -d /home/genomics/ahaegeman/working_directory`

Finally, based on the graphs, you can choose to remove the oversequenced loci using `data_selector`, selecting option 2 "select loci", 10 (Selection based on depth analysis), Y (add new range with criterion: min 32, max 300), 2 (max med_scaldep) and/or 5 (min med_dep_perc), and confirm by 13 "Run".

Here we choose 0.15 for max med_scaldep, and subsequently run data_selector again and select 1 for min med_dep_perc.

`data_selector_16.0.pl`

#### 2.4 Remove split loci

In this step we remove split loci using `data_selector`, selecting option 2 "select loci", 9 "Split loci", 0 "No split loci" and confirm by 13 "Run".

`data_selector_16.0.pl`

#### 2.5 Export genotype tables

Finally, the resulting loci can be exported to tables using the `export_tables` program. The `genotypes.txt` file will be used in the next step.

`export_tables_07.0.pl`

Once you are done, it is advised to rename the "export" folder to for example "export1". This is because once you start doing analyses, or select data, it will always start from the selected loci present in "export", and it will overwrite it with new selections.

#### 2.6 Make a histogram of the average read depth per locus

Using the `*_loci.txt` files (output of `indloc`, 1 file per sample), we can calculate the average read depth per locus over all samples. Since this information needs to be gathered for all samples, we need to link the loci using the poplocID to be able to compare the loci between the samples.
Using the shell script `combine_loci_by_poplocID.sh`, an overview file can be made that contains the necessary information to make a histogram of the average read depth per locus.

To be able to run the schell script, you need to gather all the individual loci file (files ending with "_loci.txt") in 1 folder. Next you need to specify that folder on line 6 in the shell script and save the changes. Then the shell script can be run as follows:

`bash combine_loci_by_poplocID.sh`

The output files are called `*_allinfo.txt`, 1 file for each sample. These individual files are linked together using the common poplocID by the script. A large concatenated file is made by the script containing this information for all samples, and this is called `poplocinfo_all_samples.txt`. The latter file can be read into the `R` script `histogram_read_depth_per_locus.R` where a histogram can be made of the average read depth per locus.

#### 2.7 Rarefaction analysis

To be able to assess whether or not the GBS sequencing data reached saturation, we can perform a rarefaction analysis. This means that for one or more selected samples, we do a subsampling, i.e. make copies of the preprocessed read files, but only with a fraction of the reads. We then use these subsampled read files as input for a GibPSs analysis as described above.

The reads can be subsampled using the software [`seqtk`](https://github.com/lh3/seqtk) as shown in the following example:
`seqtk sample IL108.fq 200000 > IL108_200K.fq`
`seqtk sample IL108.fq 500000 > IL108_500K.fq`
`seqtk sample IL108.fq 1000000 > IL108_1M.fq`
`seqtk sample IL108.fq 3000000 > IL108_3M.fq`

Next, a GibPSs analysis can be done using the sample and its subsampled files together. In this case we chose to lower the thresholds in the indloc step, and specifying a minimum depth of 4 instead of 10 and a character frequency threshold of 0.3.

`indloc_23.0.pl -i infiles.txt -d ind_distance_settings.txt -D 2 -mdl 4 -P 24 -r 0 -M f -c 0.3 -a 0.2`

The other steps of the GibPSs analysis are as described above. After finishing the analysis, you can calculate the common loci and allele similarity as mentioned in point 3.1. Finally, you can make plots (in Excel) of the number of loci found for a certain amount of input reads, and/or compare the allele similarity between the different files.


### 3. Calculate amount of shared loci between samples and perform clustering

#### 3.1 Calculate common loci and allele similarity
The script `parent_hybrid_comparison.py` compares all samples to each other. It uses the genotypes.txt file as generated  by the Gibpss software. Two things are calculated:
1. the number of loci in common : this gives the absolute number of loci that occur in both samples.
2. the allele similarity (in %) : this gives the percentage of loci that two samples have in common, that share at least 1 allele.
These two values are given for all one-to-one sample comparisons in the form of similarity tables: `commonloci.txt` and `snpsim.txt`.

*Example*: sample1 and sample2 have 20.000 loci in common and have an allele similarity of 5.0%. This means that 1000 loci (5% of 20.000) have an allele which occurs in both samples, without any SNPs. If there is at least 1 nucleotide difference between the alleles, the corresponding locus is not counted in the allele similarity.

`python3 parent_hybrid_comparison.py genotypes.txt`

The output of these tables can be pasted into Excel to examine the results in more detail. In section 3.2, we will also re-order the samples based on the clustering order.

#### 3.2 Cluster the samples based on presence / absence of the loci

The `genotypes.txt` file containing the information of the loci for each sample, can be read into `R` and processed with the script `Clustering.R`. In this script the following things are done:
- read files `genotypes.txt`, `commonloci.txt`, `snpsim.txt` and `annotations.txt` (a file with extra info on the samples)
- cluster the `genotypes.txt` matrix based on presence / absence of the loci by calculating the Jaccard distance between them, and performing hierarchical clustering with the method "median", which corresponds to the "Weighted Pair Group Method with Centroid Averaging (WPGMC)" method.
- use the cluster order to write reordered versions of `commonloci.txt`, `snpsim.txt` and `annotations.txt`
- cluster the `genotypes.txt` matrix based on presence / absence of the loci by the minimum evolution method, including 100 bootstraps, and write the tree to output in Newick format. The resulting tree can be visualized with treeviewing software, or online using for example [iTOL](https://itol.embl.de/upload.cgi).

#### 3.3 Visualizing trees using iTOL
iTOL or "interactive Tree of Life" can be accessed through [iTOL's website](https://itol.embl.de/). Simply go to the website and upload the tree in newick file format. This can be done by pasting the tree in the "Tree text" box, or the newick file can be directly uploaded using the "Tree file" option.

Once you have the tree, you can change the **Display mode** to "Normal" to see a rectangular tree.

If your tree contains **bootstraps**, you can add them to the tree by going to "Advanced", and then choose "Bootstraps/metadata: Display", and then choose "Text" to add the bootstraps as text.

If you have to **reroot** your tree using a certain individual, you can click that individual, choose "Tree structure" and then "reroot the tree here".

You can also **add metadata** to the tree, to color the labels or branches of the tree for example. Below, you can see the content of a text file (tab delimited).
The file below says that we want to add colors to the tree (TREE_COLORS), that the separator of the file is a tab (SEPARATOR TAB), and then that the data begins (DATA). Next, there are 5 columns, the first column being the label of the sample, the next saying what you want to color (in this case the branch), then the hex code of the color, then the line type (normal or dashed) and finally the thickness of the line (here 2). This information can be simply saved as a text file (copied from Excel), and then you can add the info to the tree by dragging and dropping the file onto the tree.
```
TREE_COLORS
SEPARATOR TAB
DATA
4714	branch	#f44336	normal	2
4774	branch	#f44336	normal	2
4775	branch	#f44336	normal	2
4776	branch	#f44336	normal	2
4777	branch	#f44336	normal	2
```
Similarly, you can choose to color the labels instead of the branches, by using "label" instead of branch, specifying the color (hex code), the text type (normal or bold) and the size (in this case 0.8).
```
TREE_COLORS
SEPARATOR TAB
DATA
4714	label	#f44336	bold	0.8
4774	label	#f44336	bold	0.8
4775	label	#f44336	bold	0.8
4776	label	#f44336	bold	0.8
4777	label	#f44336	bold	0.8
```
You can also change the labels, by having a file with the first column the current label on the tree and the second column the new label.
```
LABELS
SEPARATOR TAB
DATA
4714	4714_BEL_Jette_2014_2
4774	4774_BEL_Jette_2015_2
4775	4775_BEL_Jette_2015_2
4776	4776_BEL_Jette_2015_2
4777	4777_BEL_Jette_2015_1
```

Finally, you can save the tree by going to the "Export" tab, choosing "PDF" as "Format", choose whether you only want to export the image on the screen or the full image, and click "Export" to save.


#### 3.4 Select loci based on certain criteria

Using the `R` script `select_loci.R` we can read the genotypes.txt file and the locdata.txt file (output of Gibpss analysis) and make subselections of loci. You can for example make subselections based on the occurrence of a locus in multiple samples, the length of the locus, and the number of SNPs that occur in the locus. This script was not used yet, but can be used as starting point for future selections of group-specific markers.

### 4. Check heterozygosity

Since some samples have a known hybrid origin, it can be interesting to investigate how many alleles are present for the different loci. In this part, we will calculate how many alleles each locus has for each sample, and represent this information per sample as a stacked bar chart.
The `R` script `stacked_barplot_heterozygosity.R` reads the `genotypes.txt` file (as outputted from Gibpss) to calculate the number of alleles in each locus and makes the barplot.

This plot was eventually not used in the paper.

### 5. Concatenated alignment and phylogeny

In this part, we assume that you have succesfully run Gibpss, and have produced the output files `genotypes.txt` and `locdata.txt`. Using these files, the following scripts will make a concatenated alignment (alignment of all loci containing SNPs pasted after each other), and this concatenated alignment can be filtered for missing data, ambiguous characters and for positions where only 1 isolate deviates from the other isolates.

#### 5.1 Step 1: Make concatenated alignment of loci containing SNPs

First, we make a concatenated alignment. In this step, all loci from the `genotypes.txt` file **which contain SNPs** are concatenated and saved as fasta file. The script also takes the `locdata.txt` file from Gibpss as input to keep track of which loci and bases are printed in the alignment.
No filtering steps are done, BUT only loci are kept which have 1 or more SNPs as mentioned in the `locdata.txt` file.
An output file containing the positional information (called `info_positions_unfiltered_alignment.txt`) and a fasta file of the concatenated alignment (called `concatenated_alignment_unfiltered.fasta`) are automatically generated when running the script.

The script is run as follows:

`perl convert_genotypes_to_concatenated_alignment.pl <options>`

The following options should be used when running the script:

`-i <file>` : The input file is the `genotypes.txt` file as it is produced by the software Gibpss.

`-l <file>` : The loc file is the `locdata.txt` file as it is produced by the software Gibpss.

Make sure that all scripts and files are in the current working directory, or if not, you can refer to the script or files using their absolute or relative path.

Example:
`perl convert_genotypes_to_concatenated_alignment.pl -i genotypes.txt -l locdata.txt`

In case you want to select certain individuals from the concatenated alignment, this can be done with the perl script `get_seqs_from_fastafile.pl`, using a list of samples ("ids.txt") to be included (one sample per line):
`perl get_seqs_from_fastafile.pl ids.txt input.fa output.fa`

#### 5.2 Step 2: Filter the concatenated alignment

Next, we can filter the alignment to remove positions that are not of interest due
- to too much missing data
- too many dual bases
- "random" mutations (1 individual that deviates from others)

The script is run as follows:
`perl filter_concatenated_alignment.pl <options>`


The following options are mandatory and should be used when running the script:

`-i <file>` : The input file is the unfiltered concatenated alignment file in fasta format as generated by the script `convert_genotypes_to_concatenated_alignment.pl`.

`-p <file>` : The pos or position file is the "info_positions_unfiltered_alignment.txt" file as it is produced by the script `convert_genotypes_to_concatenated_alignment.pl`.

`-g <integer>` : Maximum number of gaps which are allowed to keep the position in the alignment, i.e. maximum number of individuals with missing data. If put at 0, no missing data is allowed. If put at the number of individuals in the dataset, all positions are allowed regardless of the number of missing data.

`-a <integer>` : Maximum number of aberrant bases which are allowed to keep the position in the alignment, i.e. maximum number of individuals with a base call which is not A,C,G,T or - (and hence is either U,R,Y,S,W,K,M,B,D,H,V,N). If put at 0, no aberrant bases are allowed. If put at the number of individuals in the dataset, all positions are allowed regardless of the number of aberrant bases.

`-o <filename>` : The file name **base** you choose as output file. Two output files are generated with this file name base: a fasta file of the filtered concatenated alignment, and an accompanying "info_positions" file with information on the kept positions. Do not add a suffix to the file name (use for example "filtered_alignment" and NOT "filtered_alignment.fasta"). Suffixes will be added by the script.


Non mandatory option:

`-s yes` : If this option is set to "yes", an extra filtering step is done to remove all positions where there is only 1 deviating base (1 sample with deviating base when compared to all other samples). If you don't want to do this, don't set the -single option at all (do not mention it on the command line).


**Example used in the paper**: Filter the alignment to remove positions where 21 or more individuals have missing data. The total number of individuals present in this dataset is 96. We do no additional filtering on "random mutations" or on degenerate bases.

`perl filter_concatenated_alignment.pl -p info_positions_unfiltered_alignment.txt -i concatenated_alignment_unfiltered.fasta -a 96 -g 21 -o filtered_alignment_a96_g21`



#### 5.3 Step 3 : Make a phylogenetic tree
There are multiple programs and algorithms to make a phylogenetic tree. Here we will use maximum likelihood using [RAXML](https://cme.h-its.org/exelixis/web/software/raxml/).

Tip: if you have a clear outlier in your dataset, make sure to put this sequence as the first sequence in your fasta file. Most phylogeny programs take the first sequence as outgroup by default.

To be able to make a tree in RAXML, we first need to convert the fasta file to phylip format. This can be done using `jmodeltest2`, giving the name of the input file to the `-d` option. Example with an input file named "concatenated_alignment.fasta":

`java -jar /bin/jmodeltest2 -d concatenated_alignment.fasta -getPhylip`

Next, we need to remove invariant SNPs since RAXML cannot cope with these. These can still occur in the data when only a selection of isolates is used in the alignment for example. We can use the `ascbias.py` script from https://github.com/btmartin721/raxml_ascbias. As input file (option `-p`) we use the phylip formatted alignment as created in the previous step. As output file (option `-o`) we can choose a name.

`ascbias.py -p concatenated_alignment.fasta.phy -o concatenated_alignment_SNPs.phy`

Finally, we can make the tree with RAXML.
Many different options and evolutionary models are available. The most important options are shown below, with two examples on how to make the tree.

`-m`: the model used

`-V`: model without rate heterogeneity (only works with a CAT model)

`-s`: input file in phylip format

`-p`:  Specify a random number seed for the parsimony inferences. This allows you to reproduce your results later.

`-x`:  Specify a random number seed and turn on rapid bootstrapping.

`-n`: base name you choose for the output files

`-T`: number of threads (processors) to be used by the server

`-w`: full path to the directory where RAXML should write the output to.

`-f`: algorithm selection, in the case of "a": rapid bootstrap analysis and search for best-scoring ML tree in one program run.

`-#`: number of bootstraps


**Example used in the paper**: GTRCAT model without rate heterogeneity (-V) and with ascertainment bias correction including 100 bootstraps (fast):

`raxml -m ASC_GTRCAT -V --asc-corr=lewis -s concatenated_alignment_SNPs.phy -p 12345 -x 12345 -n concatenated_SNPs_RAXMLtree -T 12 -w /full/path/to/output/dir -f a -# 100`

As output, RAXML will create several files starting with "RAXML_" and containing the name you specified with the `-n` option. The file you can use for visualisation (including the bootstraps) starts with "RAxML_bipartitionsBranchLabels". It is in newick format and can be visualized using iTOL as described above.

#### 5.4  Hamming distance and multidimensional scaling (MDS)

Using the filtered concatenated alignment, we can calculate the pairwise distances between the samples using the Hamming distance. With this distance, we can perform a multidimensional scaling (MDS) to project (and plot) these distances in 2 dimensions. The multidimensional scaling analysis is also known as principal coordinate analysis (PCoA). THe calculation of the distance and the MDS can be done using the R script `Hammingdistance_and_MDS_concatenated_alignment.R` with the filtered alignment containing only SNPs as input (output of `ascbias.py` in phylip format).

This plot was eventually not used in the paper.

### 6 Scatterplot of number of loci versus genome size

If genome size information is available from flow cytometry experiments, it might be interesting to check if there is a correlation with the number of loci identified by GibPSs. The script `scatterplot_numloc_vs_genomesize.R` will make a scatterplot of this relationship, using the following as input:
- the number of loci for each sample (this information is extracted in the script `Clustering.R`, see 3.2)
- a metadatafile with information on the genome size for each sample.

This plot was eventually not used in the paper.

### 7 Male vs female markers

We can try to find markers that behave differently in males and in females. To do this, we start from the filtered concatenated alignment (as described in point 5). This alignment is read into R, and there the frequencies of each base at each position is calculated. The frequencies are calculated for all individuals, but also within males and females. Next, we can try to find position/base combinations for which the marker frequencies are different between males and females. This is done with the script `filtering_alignment.R`.

Since no markers could be found that seperate the male accessions from the female accession, this analysis was not included in the paper.
